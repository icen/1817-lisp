(load "1817.lisp")
(in-package :1817)

(new-game
 #(caleb dan will richard matt tom)
 (initial-auction
  (auction :player 'caleb :sym '|minor coal mine| :price 20)
  (auction :player 'dan   :sym '|pittsburgh steel mill| :price 40))
 (stock-round
  (ipo :sym 'J :num-shares 2 :price 110 :location (locate-city "Philadelphia")))
 (operating-round
  (operate 'J
	   (loans 2)
	   (place-track (locate-city "Philadelphia") 57 '(NW SE))
	   (run-trains 1 0)
	   (buy-trains '(2T 2T)))))
