(defpackage :1817
  (:use :common-lisp)
  (:export new-game
	   initial-auction auction
	   stock-round trade ipo
	   operating-route operate place-track place-station run-trains buy-trains loans
	   merger-and-acquisition merge-companies convert-companies friendly-sale liquidate))

(in-package :1817)

;;; Utilities and table functions

(defmacro compose (state &rest fns)
  (let ((x (gensym)))
    `(let ((,x ,state))
       ,@(map 'list (lambda (f) `(setf ,x (funcall ,f ,x))) fns)
       ,x)))

(defun vec (&rest elems)
  (reduce (lambda (v x) (progn (vector-push-extend x v) v)) elems :initial-value (make-array 0 :adjustable t :fill-pointer 0)))

(defun @ (x y &optional (f #'identity) (fill 0))
    (map 'vector (lambda (i) (if i (funcall f (elt x i)) fill)) y))

(defun grade (x predicate &key (key #'elt))
  (let ((v (vec)))
    (loop for i from 0 to (1- (length x)) do
	 (vector-push-extend i v))
    (stable-sort v (lambda (a b) (funcall predicate (funcall key x a) (funcall key x b))))))

(defun where (x)
  (loop for y across x
     for p from 0
     when y
       collect p))

(defun index-of (x y)
  "Returns the first index of each element of x in y"
  (map 'vector #'(lambda (z) (position z x)) y))

(defun index-of-all (x y)
  "Finds the indices all the occurrences of x in y"
  (where (map 'vector (lambda (z) (eql x z)) y)))

(defun first-index-of (base graft)
  "Returns the index-vector into graft to make it line up element-wise with base. Both base and graft should be vectors of vectors."
  (let ((candidates (map 'vector (lambda (l r)
				   (map 'vector (lambda (x) (index-of-all x r)) l))
			 base graft)))
    (if (eq 0 (length graft))
	nil
	(map 'vector #'first
	 (reduce (lambda (acc x) (map 'vector #'intersection acc x)) candidates)))))

(defun group (x)
  (let* ((a (remove-duplicates x))
	 (b (map 'vector (lambda (y) (index-of-all y x)) a)))
    (list a b)))

(defstruct table
  (keys (vec) :type (array symbol))
  (cols (vec) :type (array symbol))
  (vals (vec) :type (array t)))

(defun define-table (keys cols)
  (make-table :keys keys :cols cols :vals (map 'vector #'identity (loop for i from 1 to (+ (length keys) (length cols)) collect (vec)))))

(defun table-columns (table)
  (concatenate 'vector (table-keys table) (table-cols table)))

(defun table-join (left right)
  "Joins the left table to the right one, via the keys on the right. Table ends with the same number of rows as the left."
  (let* ((base (@ (table-vals left) (index-of (table-columns left) (table-keys right))))
	 (graft (subseq (table-vals right) 0 (length (table-keys right))))
	 (spine (first-index-of base graft))
	 (right-vals (subseq (table-vals right) (length (table-keys right))))
	 (new-vals (map 'vector (lambda (c) (@ c spine #'identity 'nil)) right-vals)))
    (make-table
     :keys (table-keys left)
     :cols (concatenate 'vector (table-cols left) (table-cols right))
     :vals (concatenate 'vector (table-vals left) new-vals))))

(defun table-insert (table val)
  "Inserts a value into the table, overwriting the existing entry if present, or else inserting it"
  (let* ((matches (first-index-of (map 'vector #'vector (subseq val 0 (length (table-keys table))))
				  (subseq (table-vals table) 0 (length (table-keys table)))))
	 (index (if (> (length matches) 0) (elt matches 0) nil)))
    (if index
	(map 'vector (lambda (col x) (setf (elt col index) x)) (table-vals table) val)
	(map 'vector #'vector-push-extend val (table-vals table)))
    table))

(defun table-key (table new-keys)
  (let* ((columns (concatenate 'vector (table-keys table) (table-cols table)))
	 (not-keys (map 'vector #'identity (set-difference (map 'list #'identity columns)
							   (map 'list #'identity new-keys))))
	 (rearrange (index-of columns (concatenate 'vector new-keys not-keys))))
    (setf (table-keys table) new-keys)
    (setf (table-cols table) not-keys)
    (setf (table-vals table) (@ (table-vals table) rearrange))
    table))

(defun table-print (table &optional (n 50))
  (let* ((vals (map 'list (lambda (x) (subseq x 0 (min n (length (elt (table-vals table) 0))))) (table-vals table)))
	 (cols (map 'list (lambda (x) (format nil "~a" x)) (table-columns table)))
	 (widths (map 'list #'1+
		      (map 'list (lambda (hdr col)
				   (reduce (lambda (acc x) (max acc (length (format nil "~a" x)))) col :initial-value hdr))
			   (map 'list #'length cols) vals))))
    (map nil (lambda (w x) (format t "~v<~a~>" w x)) widths cols)
    (format t "~%")
    (format t "~a~%" (concatenate 'string (make-list (reduce #'+ widths :initial-value (1- (length cols))) :initial-element #\-)))
    (apply #'map (concatenate 'list
			      (list 'list
				    (lambda (&rest row)
				      (map nil (lambda (w x) (format t "~v<~a~>" w (if x x ""))) widths row)
				      (format t "~%")))
			      vals))
    table))

(defun table-lookup (table keys)
  (let ((matches (first-index-of keys table)))
    (if (> (length matches) 0) (elt matches 0) nil)))

(defun table-col (table col)
  (aref (table-vals table) (position col (table-columns table))))

;;; state definition

(defstruct state
  (phase 'initial-auction :type symbol)
  (players (vec) :type (array symbol))
  (priority-deal nil :type symbol)
  (interest-rate 5 :type fixnum)
  (balances (define-table #(name) #(balance)))
  (positions (define-table #(name sym) #(qty)))
  (prices (define-table #(sym) #(price)))
  (companies (define-table #(sym) #(size loans)))
  (trains (define-table #(sym) #(price num)))
  (map (define-table #(x y) #(tile stations tokens alias))))

(defvar *stock-prices* #(LIQ ACQ3 ACQ2 ACQ1 40 45 50 55 60 65 70 80 90 110 120 135 150 165 180 200 220 245 270 300 330 360 400 440 490 540 600))

(defun new-state ()
  (let (s (make-state))
    (map nil (lambda (s p) (table-insert (state-prices s) #(s p)))
	 #(|pittsburgh steel mill| |mountain engineers| |ohio bridge co| |union bridge co| |train station| |minor coal mine| |coal mine| |major coal mine| |minor mail contract| |mail contract| |major mail contract|)
	 #(40 40 40 80 80 30 60 90 60 90 120))
    (map nil (lambda (s p q) (table-insert (state-trains s) #(s p q)))
	 #(2T 2T+ 3T 4T 5T 6T 7T 8T)
	 #(100 100 250 400 600 750 900 1100)
	 #(100 4 12 8 5 4 3 100))
    ;; todo: map
    state))

;;; state transition functions

;; events have the following fields:
;; EVENT NAME SYM LOCATION PRICE QTY NOTES
;; event types to cover:
;; ROUND IPO PRICE TRADE STATION TILE TOKEN ACQURE

(defun obs-round (state round)
  (when (= 'SR (state-phase state))
    ;; handle end-of-sr price moves
    (let* ((owned-shares (let* ((player-owned (group (@ (table-col (state-positions state) 'sym)
							(where (map 'vector (lambda (n) (eql n (find n (state-players state))))
								    (table-col (state-positions state) 'name))))))
				(syms (first player-owned))
				(qtys (map (lambda (g) (reduce #'+ (@ (table-col (state-positions state) 'qty) g))))))
			   (make-table :keys #(sym) :cols #(owned-qty) :vals #(syms qtys))))
	   (open-market (let* ((mkt (intersection (index-of-all 'market (table-col (state-positions state) 'name))
						  (where (map 'vector (lambda (x) (< 0 x)) (table-col (state-positions state) 'qty)))))
			       (qtys (@ (table-col (state-positions state) 'qty) mkt))
			       (syms (@ (table-col (state-positions state) 'sym) mkt)))
			  (make-table :keys #(sym) :cols #(open-qty) :vals #(syms qtys))))
	   (companies (table-join (table-join (state-companies state) owned-shares)
				  open-market)))
      ;; todo: actually change share prices...
      ))
  (when (= 'OR round)
    ;; set interest rate
    )
  (when (= 'OR (state-phase state))
    ;; export a train
    )
  (setf (state-phase state) round)
  state)

(defun obs-trade (state name sym price qty)
  (let ((value   (* qty price))
	(balance (or 0 (elt (table-col (state-balances state) 'balance) (table-lookup (state-balances state) #(name)))))
	(existing-qty (or 0 (elt (table-col (state-positions state) 'qty) (table-lookup (state-positions state) #(name sym))))))
    (table-insert (state-balances state) #(name sym (- balance value)))
    (table-insert (state-positions state) #(name sym qty))
    state))

(defun obs-price (state sym price)
  (table-insert (state-prices state) #(sym price))
  state)

(defun obs-ipo (state name sym home-city price num-shares)
  ;; if num-shares is nil then this is a player
  ;; if price is nil then this is a conversion
  state)

(defun obs-station (state name location)
  state)

(defun osb-tile (state name location tile)
  ;; this should ensure that any empty station slots are present in the table as nil
  ;; so that eventual routing code can see that there is a free edge through the city
  state)

(defun obs-acquire (state name sym price)
  ;; if name is nil then this is a liquidation
  ;; if price is nil then this is a merger
  state)

;;; top level definitions
;; these populate an events table with all of the *commands* of the game
;; IPOs, trades, operations, etc. Prices are nil if irrelevant or should be supplied by the consumer

(defun new-game (&rest players)
  (let* ((cash (/ 1260 (length players)))
	 (events (define-table #() #(event name sym location price qty notes))))
    (map nil (lambda (p) (table-insert events (vec 'new-player p nil nil cash 1 "Player initialisation"))) players)
    events))

(defun auction (player sym price)
  (lambda (events) (table-insert events (vec 'trade player sym nil price 1 ""))))

(defun stock-round (&rest actions)
  (lambda (events)
    (reduce (lambda (x f) (funcall f x)) actions :initial-value (table-insert events (vec 'round nil 'SR nil nil nil "start of stock round")))))

(defun ipo (player sym price num-syms home-city &rest privates)
  (lambda (events)
    (table-insert events (vec 'ipo nil sym home-city price num-syms ""))
    (table-insert events (vec 'trade player sym nil (/ price 2) 2 "Purchase of presidential shares"))
    (map nil (lambda (p)
	       (table-insert events (vec 'trade player p nil nil 1 "IPO transfer of private companies"))
	       (table-insert events (vec 'trade sym    p nil nil 1 "Counterparty of the above")))
	 privates)
    events))

;; todo: rest of the top-level functions

(defun show-state (s)
  (table-print (state-balances s))
  (format t "~%")
  (table-print (state-positions s))
  (format t "~%")
  (table-print (state-trains s))
  (format t "~%")
  (format t "Interest rate: ~a~%" (state-interest-rate s)))

;; test

(defvar *test-game*
  '(compose
   (new-game 'caleb 'matt 'tom 'dan 'will 'jaquesy)
   (auction 'caleb '|minor coal mine| 20)
   (stock-round
    (ipo 'caleb 'S 110 2 '|New York (South)| '|minor coal mine|))))

(table-print (eval *test-game*))
