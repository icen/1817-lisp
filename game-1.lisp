(load "1817.lisp")
(in-package :1817)

(new-game
 #(will tom jaquesy matt dan caleb)
 (initial-auction
  (auction :player 'caleb   :sym '|pittsburgh steel mill| :price 40)
  (auction :player 'caleb   :sym '|mountain engineers|    :price 40)
  (auction :player 'matt    :sym '|ohio bridge co|        :price 30)
  (auction :player 'tom     :sym '|union bridge co|       :price 60)
  (auction :player 'matt    :sym '|train station|         :price 65)
  (auction :player 'caleb   :sym '|minor coal mine|       :price 25)
  (auction :player 'caleb   :sym '|coal mine|             :price 45)
  (auction :player 'dan     :sym '|major coal mine|       :price 60)
  (auction :player 'dan     :sym '|minor mail contract|   :price 50)
  (auction :player 'jaquesy :sym '|mail contract|         :price 75)
  (auction :player 'will    :sym '|major mail contract|   :price 95))
 (stock-round
  (ipo :sym 'PLE :num-shares 2 :price 140 :location (locate-city "Pittsburgh") :privates '(|mountain engineers| |pittsburgh steel mill| |coal mine|))
  (ipo :sym 'R   :num-shares 2 :price 125 :location (locate-city "Philadelphia") :buyer 'dan :privates '(|major coal mine|))))
